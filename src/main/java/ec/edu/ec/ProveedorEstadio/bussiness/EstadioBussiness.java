package ec.edu.ec.ProveedorEstadio.bussiness;

import javax.ejb.Stateless;

@Stateless
public class EstadioBussiness {
	
	public String Localidad(String localidad) throws Exception{
		int M = 10;
		int N = 100;
		if(localidad.contentEquals("General")) {
			return "G-"+Math.floor(Math.random()*(N-M+1)+M); 
		}else if (localidad.contentEquals("Tribuna")){
			return "T+"+Math.floor(Math.random()*(N-M+1)+M);
		}else if(localidad.contentEquals("Box")) {
			return "B-"+Math.floor(Math.random()*(N-M+1)+M);
		}
		return null;
	}
	

}
