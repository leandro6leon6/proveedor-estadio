package ec.edu.ec.ProveedorEstadio.ws;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import ec.edu.ec.ProveedorEstadio.bussiness.EstadioBussiness;

@WebService
public class ServicioEstadio {

	
	@Inject
	private EstadioBussiness estadio;
	
	@WebMethod
	public String Localidad(String localidad) {
		try {
			return estadio.Localidad(localidad);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return "Error: "+ e.getMessage();
		}
	}
}
